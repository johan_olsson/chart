define([
    'Models/Chart',
    'Views/ChartView'
], function (Chart, ChartView) {
    'use strict';
    
    return {
        start: function () {
            ChartView.render(Chart('revenue'));
            ChartView.render(Chart('impresions'));
            ChartView.render(Chart('visits'));
        }
    }

});