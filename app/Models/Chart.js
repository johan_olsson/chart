define(function () {

    /**
     * Using the model to provide mockdata.
     */

    return function (type) {

        var color = '#425e1d';
        var lastValue;

        switch (type) {
        case 'revenue':
            color = '#425e1d';
            break;
        case 'impresions':
            color = '#1d475e';
            break;
        case 'visits':
            color = '#5e4c1d';
            break;
        }

        var part = Math.ceil(Math.random() * 80 + 10);

        return {
            title: type.replace(/^./i, type[0].toUpperCase()),
            color: color,
            history: (function () {
                var array = [],
                    i = 0;

                for (; i < 20; i++)
                    array.push(Math.ceil(Math.random() * 30) * 10000);

                lastValue = array[array.length - 1];

                return array;
            }()),
            data: [
                {
                    label: 'Tablet',
                    value: (lastValue / 100) * part
                },
                {
                    label: 'Mobile',
                    value: (lastValue / 100) * (100 - part)
                }
            ]
        }
    }
})