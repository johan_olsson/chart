define(function () {

    return {

        shade: function (color, percent) {

            var extract = [];
            var opacity = 1;
            
            if (!color.match(/[0-9a-fA-F]{3,6}$/))
                extract = color.match(/[.\d]{1,3}/g);

            if (extract.length < 3) {

                color = color.replace('#', '');
                extract = [
                    parseInt(color.substring(0, 2), 16),
                    parseInt(color.substring(2, 4), 16),
                    parseInt(color.substring(4, 6), 16)
                ]
            }

            if (extract.length === 4)
                opacity = extract.pop();

            extract.forEach(function (v, i) {
                extract[i] = parseInt(v * (100 + percent) / 100);
                extract[i] = (extract[i] > 255) ? 255 : (extract[i] < 0) ? 0 : extract[i];
            })

            extract.push(opacity);

            return 'rgba(' + extract.join(',') + ')'
        },
        opacity: function(color, opacity) {
            
            return this.shade(color, 0).replace(/[.\d]*\)/, opacity + ')');
        
        }
    }

})