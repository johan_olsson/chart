define(function () {

    return {

        value: function (value) {
            return String(value)
                .split('').reverse().join('')
                .replace(/[\d]{3}(?!$)/g, function (m) {
                    return m + '.';
                })
                .split('').reverse().join('')
        },
        currency: function (value, currency) {
            var value = this.value(value);

            switch (currency) {
            case 'euro':
                value += '€';
                break;
            case 'dollar':
                value = '$' + value;
                break;
            }
            
            return value;
        }

    }

})