define([
    'd3',
    'Models/TextTransform',
    'Models/Color'
], function (d3, transform, Color) {
    'use strict';
    
    /**
     * This is the first time I try d3.js, So I might be
     * missing something. But in this case I would probably
     * use a tempalte to generate the svgs normally.
     */

    var baseWidth = 280;

    var w = baseWidth * 0.7,
        h = baseWidth,
        r = w / 2,
        grad = Math.PI / 180,
        totalColor = '#414141',
        baseColor = '#a6a6a6';

    return {
        render: function (options) {
            
            var total = 0;

            var themeColor = options.color;
            var history = options.history;
            var title = options.title;
            var data = options.data;
            
            var svg = d3.select('#contentWrapper')
                .append('svg:svg')
                .data([data])
                .attr('width', w)
                .attr('height', h);

            var areaGroup = svg.append('svg:g')
                .attr('transform', 'translate(20, -20)');
            var arcGroup = svg.append('svg:g')
                .attr('transform', 'translate(' + r + ',' + r + ')');
            var textGroup = svg.append('svg:g')
                .attr('transform', 'translate(' + r + ',' + w + ')');


            var arcs = arcGroup.selectAll('path')
                .data(d3.layout.pie()
                    .startAngle(0 * grad)
                    .endAngle(-360 * grad)
                    .value(function (d) {
                        total += d.value;
                        return d.value;
                    }))
                .enter();

            arcs.append('svg:path')
                .attr('fill', function (d, i) {
                    return Color.shade(themeColor, (i) ? 0 : 120);
                })
                .attr('d', d3.svg.arc()
                    .outerRadius(r)
                    .innerRadius(r - 10));

            arcGroup.append('text')
                .attr('class', 'title')
                .attr('transform', 'translate(0, -30)')
                .attr('text-anchor', 'middle')
                .text(title)
                .attr('size', '30px')
                .attr('fill', baseColor);

            arcGroup.append('text')
                .attr('class', 'total value')
                .attr('text-anchor', 'middle')
                .text(transform.currency(total, 'euro'))
                .attr('fill', totalColor)
                .attr('font-size', '30px');

            data.forEach(function (d, i) {

                textGroup.append('text')
                    .attr({
                        'text-anchor': (i) ? 'end' : 'start',
                        'x': (i) ? '-15' : '15',
                        'y': '30',
                        'font-size': '14px',
                        'fill': baseColor
                    })
                    .text(d.label)

                textGroup.append('text')
                    .attr({
                        'text-anchor': (i) ? 'end' : 'start',
                        'x': (i) ? '-30' : '15',
                        'y': '60',
                        'font-size': '24px',
                        'fill': Color.shade(themeColor, (i) ? 0 : 120)
                    })
                    .text(parseInt(d.value / total * 100))

                textGroup.append('text')
                    .attr({
                        'text-anchor': (i) ? 'end' : 'start',
                        'x': (i) ? '-15' : '44',
                        'y': '57',
                        'font-size': '14px',
                        'fill': Color.shade(themeColor, (i) ? 0 : 120)
                    })
                    .text('%')

                textGroup.append('text')
                    .attr({
                        'text-anchor': (i) ? 'end' : 'start',
                        'x': (i) ? '-15' : '15',
                        'y': '82',
                        'font-size': '12px',
                        'fill': totalColor
                    })
                    .text(transform.currency(d.value, 'euro'))
            })

            svg.append('svg:clipPath')
                .attr('id', 'clipper')
                .append('svg:circle')
                .attr('r', r - 20)
                .attr('transform', 'translate(' + parseInt(r - 20) + ',' + parseInt(r + 20) + ')');


            var interval = 500 / (history.length - 1);
            var max = Math.max.apply(Math, history);;
            var min = Math.min.apply(Math, history);
            var diff = 30 / (max - min);

            var lineData = [];
            history.forEach(function (v, i) {
                lineData.push({
                    'x': interval * i,
                    'y': 170 - diff * (v - min)
                })
            })

            var area = d3.svg.area()
                .interpolate('linear')
                .x(function (d) {
                    return x(d.x);
                })
                .y0(200)
                .y1(function (d) {
                    return y(d.y);
                });

            var x = d3.scale.linear().range([0, w - 40]);
            var y = d3.scale.linear().range([0, 150]);

            x.domain(d3.extent(lineData, function (d) {
                return d.x;
            }));

            y.domain([0, d3.max(lineData, function (d) {
                return d.y;
            })]);

            areaGroup.append('path')
                .attr('class', 'area')
                .attr('stroke', Color.shade(themeColor, 120))
                .attr('fill', Color.opacity(Color.shade(themeColor, 120), .3))
                .attr('d', area(lineData))
                .attr('clip-path', 'url(#clipper)');
            
            return {
                remove: function() {
                    svg.remove();
                }
            }
        }
    }
})
