define(['Models/Color'], function (Color) {
    describe('Color model', function () {
        it('should handle hex and rgba? format', function () {
            expect(Color.shade('#FFFFFF', 0)).toEqual('rgba(255,255,255,1)');
            expect(Color.shade('FFFFFF', 0)).toEqual('rgba(255,255,255,1)');
            expect(Color.shade('rgb(255,255,255)', 0)).toEqual('rgba(255,255,255,1)');
            expect(Color.shade('rgba(255,255,255,1)', 0)).toEqual('rgba(255,255,255,1)');
        });
        it('should brighten white to white', function () {
            expect(Color.shade('#FFFFFF', 10)).toEqual('rgba(255,255,255,1)');
        });
        it('should not lighten black', function () {
            expect(Color.shade('#000000', 10)).toEqual('rgba(0,0,0,1)');
        });
        it('should brighten colors correctly', function () {
            expect(Color.shade('rgb(201, 123, 123)', 10)).toEqual('rgba(221,135,135,1)');
        });
        it('should darken colors correctly', function () {
            expect(Color.shade('rgb(201, 123, 123)', -10)).toEqual('rgba(180,110,110,1)');
        });
        it('should keep given opacity', function () {
            expect(Color.shade('rgba(201, 123, 123, .3)', 10)).toEqual('rgba(221,135,135,.3)');
        });
        it('should set opacity correctly', function () {
            expect(Color.opacity('#FFFFFF', 0.4)).toEqual('rgba(255,255,255,0.4)');
            expect(Color.opacity('rgba(201, 123, 123, .3)', 0.4)).toEqual('rgba(201,123,123,0.4)');
        });
    });
});