define(['Models/TextTransform'], function (transform) {
    describe('TextTransform model', function () {
        it('should transform values correctly', function () {
            expect(transform.value(200000000)).toEqual('200.000.000');
        });
        it('should transform strings correctly', function () {
            expect(transform.value('200000000')).toEqual('200.000.000');
        });
        it('should transform and add euro sign after amount', function () {
            expect(transform.currency('200000000', 'euro')).toEqual('200.000.000€');
        });
        it('should transform and add dollar sign before amount', function () {
            expect(transform.currency('200000000', 'dollar')).toEqual('$200.000.000');
        });
    });
});